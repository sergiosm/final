# ENTREGA CONVOCATORIA ENERO
Sergio Sancho Martín // s.sancho.2023@alumnos.urjc.es

Enlace al video de demostración del proyecto: https://youtu.be/SaaSuO8g2RQ

Requisitos mínimos:
método change_color, método rotate_right,
método mirror, método rotate_colors, método blur, método shift,
método filter,método crop, método grayscale. 

Python files añadidos: transform_simple.py, transform_args.py y transform_multi.py.

Requisitos opcionales:
método apply_sepia (filtro a la imagen), excepciones por si ocurre algún error en los python files añadidos