import images


def change_colors(image: list[list[tuple[int, int, int]]], to_change: tuple[int, int, int], to_change_to: tuple[int, int, int]) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        listo = []
        for tupla in lista:
            if tupla == to_change:
                listo.append(to_change_to)
            else:
                listo.append(tupla)
        imagen.append(listo)

    return imagen


def rotate_right(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    pixels = []
    for lista in image:
        for tupla in lista:
            pixels.append(tupla)

    pixels_xy = []
    for x in range(height):
        pixels_xy.append([0] * width)
        for y in range(width):
            pixels_xy[x][y] = pixels[(y * height) + x]
    return pixels_xy


def mirror(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    size = len(image)
    counter = 0
    tamanio = size - 1
    for num in range(size - 1):
        counter += 1
        if counter <= size // 2:
            image[num], image[tamanio] = image[tamanio], image[num]
            tamanio -= 1

    return image


def rotate_colors(image: list[list[tuple[int, int, int]]], increment: int) -> list[list[tuple[int, int, int]]]:
    for lista in image:
        for tupla in lista:
            num_lista = image.index(lista)
            num_tupla = lista.index(tupla)
            tup1 = tupla[0] + increment
            tup2 = tupla[1] + increment
            tup3 = tupla[2] + increment
            if tup1 > 256:
                num = tup1 / 256
                value = tup1 // 256
                numero = num - value
                tup1 = int(256 * numero)
            if tup2 > 256:
                num = tup2 / 256
                value = tup2 // 256
                numero = num - value
                tup2 = int(256 * numero)
            if tup3 > 256:
                num = tup3 / 256
                value = tup3 // 256
                numero = num - value
                tup3 = int(256 * numero)

            tup1 = abs(tup1)
            tup2 = abs(tup2)
            tup3 = abs(tup3)
            image[num_lista][num_tupla] = (tup1, tup2, tup3)

    return image


def blur(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    izq = 0
    der = 0
    arr = 0
    deb = 0

    for lista in image:
        for tupla in lista:
            num_lista = image.index(lista)
            num_tupla = lista.index(tupla)
            if num_lista == 0:
                if num_tupla == 0:
                    der = image[num_lista][num_tupla + 1]
                    deb = image[num_lista + 1][num_tupla]

                elif num_tupla == len(lista) - 1:
                    izq = image[num_lista][num_tupla - 1]
                    deb = image[num_lista + 1][num_tupla]

                else:
                    izq = image[num_lista][num_tupla - 1]
                    der = image[num_lista][num_tupla + 1]
                    deb = image[num_lista + 1][num_tupla]

            elif num_lista == len(image) - 1:

                if num_tupla == 0:
                    der = image[num_lista][num_tupla + 1]
                    arr = image[num_lista - 1][num_tupla]

                elif num_tupla == len(lista) - 1:
                    izq = image[num_lista][num_tupla - 1]
                    arr = image[num_lista - 1][num_tupla]

                else:
                    izq = image[num_lista][num_tupla - 1]
                    der = image[num_lista][num_tupla + 1]
                    arr = image[num_lista - 1][num_tupla]

            elif num_tupla == 0:
                der = image[num_lista][num_tupla + 1]
                arr = image[num_lista - 1][num_tupla]
                deb = image[num_lista + 1][num_tupla]

            elif num_tupla == len(lista) - 1:
                izq = image[num_lista][num_tupla - 1]
                arr = image[num_lista - 1][num_tupla]
                deb = image[num_lista + 1][num_tupla]

            else:
                izq = image[num_lista][num_tupla - 1]
                der = image[num_lista][num_tupla + 1]
                arr = image[num_lista - 1][num_tupla]
                deb = image[num_lista + 1][num_tupla]

            if izq == 0:
                if arr == 0:
                    sum1 = tupla[0] + der[0] + deb[0]
                    sum2 = tupla[1] + der[1] + deb[1]
                    sum3 = tupla[2] + der[2] + deb[2]
                    new_tupla = (sum1 // 3, sum2 // 3, sum3 // 3)
                    image[num_lista][num_tupla] = new_tupla
                elif deb == 0:
                    sum1 = tupla[0] + der[0] + arr[0]
                    sum2 = tupla[1] + der[1] + arr[1]
                    sum3 = tupla[2] + der[2] + arr[2]
                    new_tupla = (sum1 // 3, sum2 // 3, sum3 // 3)
                    image[num_lista][num_tupla] = new_tupla
                else:
                    sum1 = tupla[0] + der[0] + arr[0] + deb[0]
                    sum2 = tupla[1] + der[1] + arr[1] + deb[1]
                    sum3 = tupla[2] + der[2] + arr[2] + deb[2]
                    new_tupla = (sum1 // 4, sum2 // 4, sum3 // 4)
                    image[num_lista][num_tupla] = new_tupla

            elif der == 0:
                if arr == 0:
                    sum1 = tupla[0] + izq[0] + deb[0]
                    sum2 = tupla[1] + izq[1] + deb[1]
                    sum3 = tupla[2] + izq[2] + deb[2]
                    new_tupla = (sum1 // 3, sum2 // 3, sum3 // 3)
                    image[num_lista][num_tupla] = new_tupla
                elif deb == 0:
                    sum1 = tupla[0] + izq[0] + arr[0]
                    sum2 = tupla[1] + izq[1] + arr[1]
                    sum3 = tupla[2] + izq[2] + arr[2]
                    new_tupla = (sum1 // 3, sum2 // 3, sum3 // 3)
                    image[num_lista][num_tupla] = new_tupla
                else:
                    sum1 = tupla[0] + izq[0] + arr[0] + deb[0]
                    sum2 = tupla[1] + izq[1] + arr[1] + deb[1]
                    sum3 = tupla[2] + izq[2] + arr[2] + deb[2]
                    new_tupla = (sum1 // 4, sum2 // 4, sum3 // 4)
                    image[num_lista][num_tupla] = new_tupla
            elif arr == 0:
                sum1 = tupla[0] + izq[0] + der[0] + deb[0]
                sum2 = tupla[1] + izq[1] + der[1] + deb[0]
                sum3 = tupla[2] + izq[2] + der[2] + deb[0]
                new_tupla = (sum1 // 4, sum2 // 4, sum3 // 4)
                image[num_lista][num_tupla] = new_tupla
            elif deb == 0:
                sum1 = tupla[0] + izq[0] + der[0] + arr[0]
                sum2 = tupla[1] + izq[1] + der[1] + arr[1]
                sum3 = tupla[2] + izq[2] + der[2] + arr[2]
                new_tupla = (sum1 // 4, sum2 // 4, sum3 // 4)
                image[num_lista][num_tupla] = new_tupla
            else:
                sum1 = tupla[0] + izq[0] + der[0] + arr[0] + deb[0]
                sum2 = tupla[1] + izq[1] + der[1] + arr[1] + deb[1]
                sum3 = tupla[2] + izq[2] + der[2] + arr[2] + deb[2]
                new_tupla = (sum1 // 5, sum2 // 5, sum3 // 5)
                image[num_lista][num_tupla] = new_tupla
    return image


def shift(image: list[list[tuple[int, int, int]]], horizontal: int, vertical: int) -> list[list[tuple[int, int, int]]]:
    width, height = images.size(image)
    num = abs(horizontal)
    nums = abs(vertical)
    listos = images.create_blank(num, height)
    if horizontal >= 0:
        imagen = listos + image

    else:
        imagen = image + listos

    for lista in imagen:
        for xx in range(nums):
            if vertical >= 0:
                lista.append((0, 0, 0))
            else:
                lista.insert(0, (0, 0, 0))

    return imagen


def filter(image: list[list[tuple[int, int, int]]], r: float, g: float, b: float) -> list[list[tuple[int, int, int]]]:
    imagen = []
    for lista in image:
        listo = []
        for tupla in lista:
            tup1 = tupla[0] * r
            tup2 = tupla[1] * g
            tup3 = tupla[2] * b
            if tup1 > 255:
                tup1 = 255
            if tup2 > 255:
                tup2 = 255
            if tup3 > 255:
                tup3 = 255

            tupla = (int(tup1), int(tup2), int(tup3))
            listo.append(tupla)
        imagen.append(listo)
    return imagen


def crop(image: list[list[tuple[int, int, int]]], x: int, y: int, width: int, height: int) -> list[list[tuple[int, int, int]]]:
    max_x = x + width
    max_y = y + height
    imagen = []
    for algo in range(x, max_x):
        lista = []
        for todo in range(y, max_y):
            valor = image[algo][todo]
            lista.append(valor)

        imagen.append(lista)

    return imagen


def grayscale(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_image = []
    for lista in image:
        listo = []
        for tupla in lista:
            new_val = int((tupla[0] + tupla[1] + tupla[2]) // 3)
            if new_val > 255:
                new_val = 255
            new_tupla = (new_val, new_val, new_val)
            listo.append(new_tupla)

        new_image.append(listo)
    return new_image

def apply_sepia(image: list[list[tuple[int, int, int]]]) -> list[list[tuple[int, int, int]]]:
    new_image = []
    for lista in image:
        listo = []
        for tupla in lista:
            r, g, b = tupla
            new_r = int(0.393 * r + 0.769 * g + 0.189 * b)
            new_g = int(0.349 * r + 0.686 * g + 0.168 * b)
            new_b = int(0.272 * r + 0.534 * g + 0.131 * b)
            new_tupla = (min(255, new_r), min(255, new_g), min(255, new_b))
            listo.append(new_tupla)
        new_image.append(listo)
    return new_image

