import sys
import images
import transforms


def main():
    try:
        nombre = sys.argv[1]
    except IndexError:
        print("Error.... El nombre del archivo que has introducido no es un nombre de archivo.")
        sys.exit(1)
    try:
        algo = nombre.split(".jpg")
    except:
        print("Error.... El archivo que has introducido no tiene extension .jpg")
        sys.exit(1)

    opcion = sys.argv[2]
    image = images.read_img(nombre)

    if opcion == "change_colors":
        try:
            num1 = int(sys.argv[3])
            num2 = int(sys.argv[4])
            num3 = int(sys.argv[5])
            num4 = int(sys.argv[6])
            num5 = int(sys.argv[7])
            num6 = int(sys.argv[8])
        except IndexError:
            print("Error... Alguno de los valores que has introducido, no son correctos o no son numeros enteros. ")
            sys.exit(1)
        tup1 = (num1, num2, num3)
        tup2 = (num4, num5, num6)
        imagen = transforms.change_colors(image, tup1, tup2)

    elif opcion == "rotate_right":
        imagen = transforms.rotate_right(image)

    elif opcion == "mirror":
        imagen = transforms.mirror(image)

    elif opcion == "rotate_colors":
        try:
            incremento = int(sys.argv[3])
        except IndexError:
            print("Error... Debes introducir un número para el incremento de colores.")
            sys.exit(1)
        imagen = transforms.rotate_colors(image, incremento)

    elif opcion == "blur":
        imagen = transforms.blur(image)

    elif opcion == "filter":
        try:
            num1 = float(sys.argv[3])
            num2 = float(sys.argv[4])
            num3 = float(sys.argv[5])

        except IndexError:
            print("Error... Alguno de los valores que has introducido, no son numeros o con decimales. ")
            sys.exit(1)
        imagen = transforms.filter(image, num1, num2, num3)

    elif opcion == "crop":
        try:
            value_x = int(sys.argv[3])
            value_y = int(sys.argv[4])
            ancho = int(sys.argv[5])
            alto = int(sys.argv[6])

        except IndexError:
            print("Error... Faltan valores para recortar la imagen. Debes introducir x, y, ancho y alto.")
            sys.exit(1)
        imagen = transforms.crop(image, value_x, value_y, ancho, alto)

    elif opcion == "shift":
        try:
            horizontal = int(sys.argv[3])
            vertical = int(sys.argv[4])
        except IndexError:
            print("Error... Debes introducir valores enteros para el desplazamiento de la imagen.")
            sys.exit(1)
        imagen = transforms.shift(image, horizontal, vertical)

    elif opcion == "grayscale":
        imagen = transforms.grayscale(image)
    elif opcion == "apply_sepia":
        imagen = transforms.apply_sepia(image)
    else:
        print("Error... El nombre de la función que has introducido es incorrecto. Intentalo de nuevo.")
        sys.exit(1)

    images.write_img(imagen, f"{algo[0]}_trans.jpg")

if __name__ == '__main__':
    main()