import sys
import images
import transforms


def main():
    try:
        nombre = sys.argv[1]
    except IndexError:
        print("Error.... El nombre del archivo que has introducido no es un nombre de archivo.")
        sys.exit(1)
    try:
        algo = nombre.split(".jpg")
    except:
        print("Error.... El archivo que has introducido no tiene extension .jpg")
        sys.exit(1)

    opcion = sys.argv[2]
    image = images.read_img(nombre)

    if opcion == "rotate_right":
        imagen = transforms.rotate_right(image)
    elif opcion == "mirror":
        imagen = transforms.mirror(image)
    elif opcion == "blur":
        imagen = transforms.blur(image)
    elif opcion == "grayscale":
        imagen = transforms.grayscale(image)
    elif opcion == "apply_sepia":
        imagen = transforms.apply_sepia(image)

    else:
        print("Error... El nombre de la función que has introducido es incorrecto. Intentalo de nuevo.")
        sys.exit(1)

    images.write_img(imagen, f"{algo[0]}_trans.jpg")


if __name__ == '__main__':
    main()
